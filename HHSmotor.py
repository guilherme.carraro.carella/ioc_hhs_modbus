from ophyd import PVPositioner, Component, Device, EpicsSignal, EpicsSignalRO


# class Motor():


class SpeedController(PVPositioner):
    
    setpoint = Component(EpicsSignal, "speedRef_Rd", write_pv = "speedRef_Wr",  kind="normal")
    readback = Component(EpicsSignalRO, "speedFeedback", kind = "hinted")
    done = Component(EpicsSignalRO, "ActualBusy", kind = "omitted")
    put_complete = True
    
    acceleration = Component(EpicsSignal, "accel_Rd", write_pv = "accel_Wr", kind = "config")
    deceleration = Component(EpicsSignal, "decel_Rd", write_pv = "decel_Wr", kind = "config")
    
    
class Motor(Device):
    
    speedController = Component(SpeedController,"")
    
    drvEnable = Component(EpicsSignal, "drvEnable_Rd", write_pv = "drvEnable_Wr", kind = "omitted")
    
    def stage(self):
        self.drvEnable.set(1).wait()
        super().stage()
        
    def unstage(self):
        self.drvEnable.set(0).wait()
        super().unstage()

    

# class MotorStatus(Device):

# 	hardwareEnable 	= Component(EpicsSignalRO, "hardwareEnable", kind="config")
# 	drvEnable 		= Component(EpicsSignalRO, "drvEnable_Rd", kind="config")
# 	runForward 		= Component(EpicsSignalRO, "runForward_Rd", kind="config")
#     rampEnable      = Component(EpicsSignalRO, "rampEnabel_Rd", kind="config")
#     sRampEnable     = Component(EpicsSignalRO, "sRampEnable_Rd", kind="config")
    
# 	acceleration 	= Component(EpicsSignalRO, "accel_Rd", kind="normal")
# 	deceleration 	= Component(EpicsSignalRO, "decel_Rd", kind="normal")
# 	speedSetpoint 	= Component(EpicsSignalRO, "speedRef_Rd", kind="normal")
# 	speedFeedback 	= Component(EpicsSignalRO, "speedFeedback", kind="hinted")
# 	posFeedback 	= Component(EpicsSignalRO, "posFeedback", kind="hinted")
#   sRampRate       = Component(EpicsSignalRO, "sRampRate_Rd", kind="normal")

# class MotorControl(Device):

# 	def __init__(self, prefix, *args, **kwargs):
# 		super().__init__(prefix,**kwargs) # ???? Why ????
    
# 	drvEnable 		= Component(EpicsSignal, "drvEnable_Wr", kind="config")
# 	runForward 		= Component(EpicsSignal, "runForward_Wr", kind="config")
#     rampEnable      = Component(EpicsSignal, "rampEnable_Wr", kind="config")
#     sRampEnable     = Component(EpicsSignal, "sRampEnable_Wr", kind="config")
    
# 	accelStp 		= Component(EpicsSignal, "accel_Wr", kind="normal")
# 	decelStp 		= Component(EpicsSignal, "decel_Wr", kind="normal")
# 	speedStp 		= Component(EpicsSignal, "speedRef_Wr", kind="normal")
#     sRampRate       = Component(EpicsSignal, "sRampRate_Wr", kind="normal")

# class Motor(Device):

# 	status = Component(MotorStatus, "")
# 	control = Component(MotorControl, "")

