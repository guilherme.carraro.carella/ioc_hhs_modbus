#!../../bin/linux-x86_64/HHS_MCi210_Modbus

#- You may have to change HHS_MCi210_Modbus to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/HHS_MCi210_Modbus.dbd"
HHS_MCi210_Modbus_registerRecordDeviceDriver pdbbase

drvAsynIPPortConfigure("modbusTCP", "193.149.14.226:502", 0, 0, 0)
asynSetOption("modbusTCP", 0, "disconnectOnReadTimeout", "Y")

modbusInterposeConfig("modbusTCP", 0, 2000, 0)

# AMC Advanced Motion Controller (pendent)



# Position Feedback
drvModbusAsynConfigure("rotFeedback", "modbusTCP", 0, 3, 327, 1, UINT16, 10, "ControlTechniques")	#M03.P028
drvModbusAsynConfigure("posFeedback", "modbusTCP", 0, 3, 328, 1, UINT16, 1, "ControlTechniques")	#M03.P028
drvModbusAsynConfigure("fineposFeedback", "modbusTCP", 0, 3, 329, 1, UINT16, 1, "ControlTechniques")#M03.P030

# Ramp Controller
drvModbusAsynConfigure("rampEnable_Rd", "modbusTCP", 0, 3, 201, 1, UINT16, 100, "ControlTechniques") #M02.P02
drvModbusAsynConfigure("rampEnable_Wr", "modbusTCP", 0, 6, 201, 1, UINT16, 1, "ControlTechniques") #M02.P02
drvModbusAsynConfigure("sRampEnable_Rd", "modbusTCP", 0, 3, 205, 1, UINT16, 100, "ControlTechniques") #M02.P06
drvModbusAsynConfigure("sRampEnable_Wr", "modbusTCP", 0, 6, 205, 1, UINT16, 1, "ControlTechniques") #M02.P06
drvModbusAsynConfigure("sRampRate_Rd", "modbusTCP", 0, 3, 206, 1, UINT16, 100, "ControlTechniques") #M02.P07
drvModbusAsynConfigure("sRampRate_Wr", "modbusTCP", 0, 6, 206, 1, UINT16, 1, "ControlTechniques") #M02.P07
drvModbusAsynConfigure("accel_Rd", "modbusTCP", 0, 3, 210, 1, UINT16, 100, "ControlTechniques") #M02.P011
drvModbusAsynConfigure("accel_Wr", "modbusTCP", 0, 6, 210, 1, UINT16, 1, "ControlTechniques") #M02.P011
drvModbusAsynConfigure("decel_Rd", "modbusTCP", 0, 3, 220, 1, UINT16, 100, "ControlTechniques") #M02.P021
drvModbusAsynConfigure("decel_Wr", "modbusTCP", 0, 6, 220, 1, UINT16, 1, "ControlTechniques") #M02.P021

# Speed Controller
drvModbusAsynConfigure("speedRef_Rd", "modbusTCP", 0, 3, 120, 1, UINT16, 100, "ControlTechniques")	#M01.P021
drvModbusAsynConfigure("speedRef_Wr", "modbusTCP", 0, 6, 120, 1, UINT16, 1, "ControlTechniques")	#M01.P021
drvModbusAsynConfigure("speedFeedback", "modbusTCP", 0, 3, 301, 1, UINT16, 1, "ControlTechniques")	#M03.P001
drvModbusAsynConfigure("atSpeed", "modbusTCP", 0, 3, 1005, 1, UINT16, 1, "ControlTechniques") #M10.P06

# Unidrive Status
drvModbusAsynConfigure("unidriveReset_Rd", "modbusTCP", 0, 3, 1032, 1, UINT16, 100, "ControlTechniques") #M10.P033
drvModbusAsynConfigure("unidriveReset_Wr", "modbusTCP", 0, 6, 1032, 1, UINT16, 1, "ControlTechniques") #M10.P033
drvModbusAsynConfigure("drvEnable_Rd", "modbusTCP", 0, 3, 614, 1, UINT16, 100, "ControlTechniques")	#M06.P013
drvModbusAsynConfigure("drvEnable_Wr", "modbusTCP", 0, 6, 614, 1, UINT16, 1, "ControlTechniques") #M06.P013
drvModbusAsynConfigure("runForward_Rd", "modbusTCP", 0, 3, 629, 1, UINT16, 100, "ControlTechniques") #M06.P030
drvModbusAsynConfigure("runForward_Wr", "modbusTCP", 0, 6, 629, 1, UINT16, 1, "ControlTechniques") #M06.P030
drvModbusAsynConfigure("hardwareEnable", "modbusTCP", 0, 3, 628, 1, UINT16, 100, "ControlTechniques") #M06.P029

# PLC
drvModbusAsynConfigure("cmdRun_Rd", "modbusTCP", 0, 3, 1830, 1, UINT16, 100, "ControlTechniques") #M18.P031
drvModbusAsynConfigure("cmdRun_Wr", "modbusTCP", 0, 6, 1830, 1, UINT16, 1, "ControlTechniques") #M18.P031
drvModbusAsynConfigure("posTriggered_Rd", "modbusTCP", 0, 3, 1810, 1, UINT16, 100, "ControlTechniques") #M18.P011
drvModbusAsynConfigure("stepSize_Rd", "modbusTCP", 0, 3, 1811, 1, UINT16, 100, "ControlTechniques") #M18.P012
drvModbusAsynConfigure("stepSize_Wr", "modbusTCP", 0, 6, 1811, 1, UINT16, 1, "ControlTechniques") #M18.P012

# MCi210 Status
drvModbusAsynConfigure("programStatus", "modbusTCP", 0, 3, 1636, 1, UINT16, 100, "ControlTechniques") #M16.P037
drvModbusAsynConfigure("resetMCi_Rd", "modbusTCP", 0, 3, 1606, 1, UINT16, 100, "ControlTechniques") #M16.P07
drvModbusAsynConfigure("resetMCi_Wr", "modbusTCP", 0, 6, 1606, 1, UINT16, 1, "ControlTechniques") #M16.P07

## Load record instances
dbLoadRecords("db/motor.db","user=hhs")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=hhs"
