## set base image debian
FROM registry.hzdr.de/hzb/epics/base/ubuntu_20_04:1.1.0 AS base
## set default shell for next commands
SHELL ["/bin/bash", "-c"]

## Install dependencies
RUN apt-get update && apt-get install -yq git build-essential

# prepare environment
RUN mkdir -p /opt/epics/support && \
    mkdir -p /opt/epics/ioc && \
    export SUPPORT=/opt/epics/support && \
    git config --global advice.detachedHead false && \
    echo "export LC_ALL=C" >> ~/.bashrc && \
    export LC_ALL=C

# configure paths
COPY RELEASE.local ${SUPPORT}/RELEASE.local
COPY RELEASE.local /opt/epics/ioc/RELEASE.local

# install autosave 
RUN git clone --depth 1 --recursive --branch R5-10-2 https://github.com/epics-modules/autosave.git ${SUPPORT}/autosave && \
    make -C ${SUPPORT}/autosave -j $(nproc)

# install seq
RUN git clone --depth 1 --recursive --branch vendor_2_2_8 https://github.com/ISISComputingGroup/EPICS-seq.git ${SUPPORT}/seq && \
    make -C ${SUPPORT}/seq -j $(nproc)

# install sscan 
RUN git clone --depth 1 --recursive --branch R2-11-5 https://github.com/epics-modules/sscan.git ${SUPPORT}/sscan && \
    make -C ${SUPPORT}/sscan -j $(nproc)

# install calc
RUN git clone --depth 1 --recursive --branch R3-7-4 https://github.com/epics-modules/calc.git ${SUPPORT}/calc && \
    make -C ${SUPPORT}/calc -j $(nproc)

# install asyn
RUN git clone --depth 1 --recursive --branch R4-44-2 https://github.com/epics-modules/asyn.git ${SUPPORT}/asyn && \
    make -C ${SUPPORT}/asyn -j $(nproc)

# install busy
RUN git clone --depth 1 --recursive --branch R1-7-4 https://github.com/epics-modules/busy.git ${SUPPORT}/busy && \
    make -C ${SUPPORT}/busy -j $(nproc)

# install modbus
RUN git clone --depth 1 --recursive https://github.com/epics-modules/modbus.git ${SUPPORT}/modbus && \ 
    make -C ${SUPPORT}/modbus -j $(nproc)